package device

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

/*
	获取设备属性列表(获取设备实时数据)

参数：

	url：iot项目的地址信息（ip:port）
	id:设备ID
	roleId:角色ID，可以为空
	token:令牌

返回值：

	请求的数据结果
*/
func QueryDeviceData(url string, id string, roleId []string, token string) string {
	baseURL := "http://" + url + "/api/wsat-service-device/devices/" + id + "/propertys"

	req, err := http.NewRequest("GET", baseURL, nil)
	if err != nil {
		return fmt.Sprintf("Error creating request:%d", err)
	}

	// 设置请求头
	req.Header.Set("Authorization", token)

	// 设置查询参数
	q := req.URL.Query()
	for _, id := range roleId {
		q.Add("roleId", id)
	}
	req.URL.RawQuery = q.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Sprintf("Error sending request:%d", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Sprintf("Error reading response:%d", err)
	}

	return string(body)
}

/*
	查询设备历史数据(iotdb)

参数：

	url：iot项目的地址信息（ip:port）
	deviceLabel:设备唯一标识符
	productCode:产品编码
	startTime:开始时间（时间戳）
	endTime:结束时间（时间戳）
	pageNum:页码
	pageSize:页内数量
	token:令牌

返回值：

	请求的数据结果
*/
func QueryDeviceHistoryDataForIotdb(url string, deviceLabel string, productCode string, startTime string, endTime string, pageNum int, pageSize int, token string) string {
	baseURL := "http://" + url + "/api/wsat-service-device/devices/getDeviceHistoryDataForIotdb" // 替换为实际的接口URL

	// 构建请求URL
	req, err := http.NewRequest("GET", baseURL, nil)
	if err != nil {
		return fmt.Sprintf("Error creating request:%d", err)
	}

	// 设置请求头
	req.Header.Set("Authorization", token)

	// 添加查询参数
	q := req.URL.Query()
	q.Add("deviceLabel", deviceLabel)
	q.Add("productCode", productCode)
	q.Add("startTime", startTime)
	q.Add("endTime", endTime)
	q.Add("pageNum", fmt.Sprintf("%d", pageNum))
	q.Add("pageSize", fmt.Sprintf("%d", pageSize))
	req.URL.RawQuery = q.Encode()

	// 发送请求
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Sprintf("Error sending request:%d", err)
	}
	defer resp.Body.Close()

	// 读取响应内容
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Sprintf("Error reading response:%d", err)
	}

	return string(body)
}
