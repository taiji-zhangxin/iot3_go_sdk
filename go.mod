module gitee.com/taiji-zhangxin/iot3_go_sdk

go 1.21

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	golang.org/x/net v0.19.0 // indirect
)
